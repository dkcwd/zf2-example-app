#!/bin/sh
DB_USER=$APP_DB_USER
DB_PASS=$APP_DB_PASS
DB_HOST=$APP_DB_HOST
# update with environment specific details
mysql -u $DB_USER -p$DB_PASS -h $DB_HOST -e 'drop database if exists test_zf2_example_app'
mysql -u $DB_USER -p$DB_PASS -h $DB_HOST -e 'create database test_zf2_example_app'
mysql -u $DB_USER -p$DB_PASS -h $DB_HOST test_zf2_example_app < ./schema.sql
mysql -u $DB_USER -p$DB_PASS -h $DB_HOST test_zf2_example_app < ./data.sql
