#!/bin/sh
# before running tests for the first time cd to the project tests directory and run:
#     source config/environment.sh
# followed by:
#     init.sh

cd ../
./init.sh

cp config/local.config.php tests/config/local.config.php
