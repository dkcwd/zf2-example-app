<?php

namespace ApplicationTests\Assets;

use \PHPUnit_Framework_TestCase;

class ApplicationTest extends \PHPUnit_Framework_TestCase
{
    public function setUp() {}

    /**
     * Tests whether the oauth extension is loaded
     */
    public function testOauthExtensionIsLoaded()
    {
        $this->assertTrue(extension_loaded('oauth'), 'The Oauth extension must be loaded');
    }

}