# features/home.feature
Feature: home
  Any visitor can access the homepage

  Scenario: Visiting the home page as a guest
    Given I go to "/"
    Then I should be on the homepage
    And the response should contain "Some Navigation here soon"
    And the response should contain "ZF2 Example Application"
    And the response should contain "Content is required"