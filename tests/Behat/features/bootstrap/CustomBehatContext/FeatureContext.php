<?php

namespace CustomBehatContext;
use Behat\MinkExtension\Context\MinkDictionary;

/**
 * Features context.
 */
class FeatureContext extends BaseFeatureContext
{
    use MinkDictionary;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        $this->getMink();
    }

    /**
     * @Given /^I have done something with "([^"]*)"$/
     */
    public function iHaveDoneSomethingWith($argument)
    {
        doSomethingWith($argument);
    }

}
