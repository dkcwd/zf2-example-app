#!/bin/sh
clear
## run any required db setup
cd setup
./setup.sh
cd ../

## run PHPUnit tests
../vendor/bin/phpunit --coverage-html coverage/ --configuration ./config/all-tests.xml
## run coveralls to get unit test code coverage
#../vendor/bin/coveralls -v

# run Behat tests
cd Behat
./run.sh