#!/bin/sh
# define environment specific values here
export APP_ENVIRONMENT="testing"
export APP_RELEASE_VERSION="dev-master"
export APP_PROJECT_PATH="\/home\/davdev\/Projects\/zf2-example-app"
## DB
export APP_DB_NAME="test_zf2_example_app"
export APP_DB_USER="root"
export APP_DB_PASS="root"
export APP_DB_HOST="127.0.0.1"
