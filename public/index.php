<?php
ini_set('display_errors', 1);
// Environment and version variables should be replaced with actual hard coded values upon deployment.
$appEnv = 'testing';
$appVer = 'dev-master';
$projectPath = '/home/davdev/Projects/zf2-example-app';

// If not a deployed version (e.g. working locally)
$appEnv = (strpos($appEnv, '$') !== 0) ? $appEnv : getenv('APP_ENVIRONMENT');
$appVer = (strpos($appVer, '$') !== 0) ? $appVer : 'Version not available';

defined('APP_ENVIRONMENT') || define('APP_ENVIRONMENT', $appEnv);
defined('APP_RELEASE_VERSION') || define('APP_RELEASE_VERSION', $appVer);
defined('APP_PATH') || define('APP_PATH', $projectPath . '/');
defined('PHPUNIT_RUNNING') || define('PHPUNIT_RUNNING', false);

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

require APP_PATH . 'vendor/autoload.php';

$appConfig = require APP_PATH . 'config/application.config.php';

if (stream_resolve_include_path(APP_PATH . 'config/development.config.php')) {
    $appConfig = \Zend\Stdlib\ArrayUtils::merge($appConfig, require APP_PATH . 'config/development.config.php');
}

Zend\Mvc\Application::init($appConfig)->run();