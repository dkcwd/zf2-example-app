<?php

/**
 * Set environment specific params
 */
switch (APP_ENVIRONMENT) {
    case 'development':
        $displayErrors = true;
        break;
    default:
        /**
         * e.g. Production environment
         */
        $displayErrors = false;
        break;
}

return array(
    'view_manager' => array(
        'display_not_found_reason' => $displayErrors,
        'display_exceptions'       => $displayErrors,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'layout'                   => 'layout/default',
        'template_map' => array(
            'layout/default'          => __DIR__ . '/../view/layout/default.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);