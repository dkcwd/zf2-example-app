<?php

/**
 * Set environment specific params
 */
switch (APP_ENVIRONMENT) {
    case 'development':
        $assetManagerCacheConfig = array(
            'default' => array(
                'cache'     => 'Filesystem',
                'options' => array (
                    'dir' => APP_PATH . 'public/cache/assetic',
                ),
            )
        );
        break;
    default:
        /**
         * e.g. Production environment
         */
        $assetManagerCacheConfig = array(
            'default' => array(
                'cache'     => 'FilePath',
                'options' => array (
                    'dir' => APP_PATH . 'public',
                ),
            )
        );
        break;
}

return array(
    'asset_manager' => array(
        /**
         * Cache
         */
        'caching' => $assetManagerCacheConfig,
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../public',
            ),
            'collections' => array(
                'css/less.css' => array(
                    'less/bootstrap.css',
                    'less/yamm.css',
                    'less/font-awesome.css',
                ),
                'css/less-ie.css' => array(
                    'less/ie.less',
                ),
                'css/less-ie8.css' => array(
                    'less/ie8.less',
                ),
                'css/less-ie9.css' => array(
                    'less/ie9.less',
                ),
                'js/global.js' => array(
                    'vendor/lodash/dist/lodash.js',
                    'vendor/json3/lib/json3.js',
                    'vendor/jquery/dist/jquery.js',
                    'vendor/bootstrap/dist/js/bootstrap.js',
                    'vendor/respond/dest/respond.src.js',
                    'vendor/jquery.scrollTo/jquery.scrollTo.js',
                    'vendor/jquery.tablesorter/js/jquery.tablesorter.js',
                    'vendor/jquery.serializeJSON/jquery.serializeJSON.js',
                    'vendor/moment/moment.js',
                    'vendor/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
                    'vendor/blockui/jquery.blockUI.js',
                    'vendor/noty/js/noty/packaged/jquery.noty.packaged.js',
                    'vendor/jquery-fileinput/dist/jquery.fileinput.js',
                ),
            ),
        ),
        /**
         * Filters
         */
        'filters' => array(
            /**
             * CSS
             */
            'css/less.css' => array(
                array(
                    'filter' => 'Lessphp'
                ),
            ),
        ),
    ),
);