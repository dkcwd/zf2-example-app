<?php

return array(
    'controllers' => array(
        'factories' => array(
            'Application\Controller\Index' => 'Application\Controller\Service\IndexControllerFactory',
        ),
    ),
);