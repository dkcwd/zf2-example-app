<?php

return array(
    'zfc_rbac' => array(
        'guards' => array(
            'ZfcRbac\Guard\RouteGuard' => array(
                'home' => array('guest'),
            ),
        ),
    ),
);