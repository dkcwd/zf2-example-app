<?php

return array(
    'session' => array(
        'config' => array(
            'name' => 'ZF2_EXAMPLE_APP',
        ),
        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
    ),
);