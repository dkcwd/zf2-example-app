<?php

namespace Application;

use Zend\Console\Console;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;

class Module implements ConfigProviderInterface, BootstrapListenerInterface
{
    /**
     * @return array|mixed|\Traversable
     */
    public function getConfig()
    {
        return array_merge_recursive(
            include __DIR__ . '/config/asset-manager.config.php',
            include __DIR__ . '/config/controllers.config.php',
            include __DIR__ . '/config/language.config.php',
            include __DIR__ . '/config/rbac.config.php',
            include __DIR__ . '/config/router.config.php',
            include __DIR__ . '/config/services.config.php',
            include __DIR__ . '/config/session.config.php',
            include __DIR__ . '/config/view.config.php'
        );
    }

    /**
     * @param EventInterface $e
     * @return array|void
     */
    public function onBootstrap(EventInterface $e)
    {
        if (Console::isConsole()) {
            return;
        }

        // Add a header to the response which displays the app release version
        if ($e instanceof \Zend\Mvc\MvcEvent) {
            $headers = $e->getResponse()->getHeaders();
            $headers->addHeaderLine('Release-Environment: ' . APP_ENVIRONMENT);
            $headers->addHeaderLine('Release-Version: ' . APP_RELEASE_VERSION);
        }
    }
}