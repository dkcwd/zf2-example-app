#!/bin/sh
# before running the application for the first time cd to the project root and run:
#     source config/environment.sh
# followed by:
#     init.sh

## remove existing public index.php file and replace it
cd public
rm index.php
cp index.php.dist index.php

# APP_ENVIRONMENT and APP_RELEASE_VERSION vars
sed -i 's/$DEPLOYMENT_ENVIRONMENT/'$APP_ENVIRONMENT'/g' index.php
sed -i 's/$DEPLOYMENT_VERSION/'$APP_RELEASE_VERSION'/g' index.php
sed -i 's/$DEPLOYMENT_PROJECT_PATH/'$APP_PROJECT_PATH'/g' index.php

# remove existing local test environment config file and replace it
cd ../config
rm local.config.php
cp ci.config.php.dist local.config.php

## Swap placeholder values for environment variable values
cd ../
./env_vars.sh
