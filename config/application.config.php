<?php

switch (APP_ENVIRONMENT) {
    case 'testing':
        $staticConfigPaths = array(APP_PATH . '/tests/config/local.config.php');
        break;
    default:
        $staticConfigPaths = array(APP_PATH . '/config/local.config.php');
}

return array(
    'modules' => array(
        'Application',
        'AssetManager',
    ),
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor'
        ),
        'config_static_paths' => $staticConfigPaths
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
);
