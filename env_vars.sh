#!/bin/sh
## NOTE: Always add new placeholder vars from CI config which need to be swapped out below this line
## Value on the left is the placeholder you want to replace, value on the right is used to replace the placeholder

# go to config directory
cd config

# DB vars
sed -i 's/$DB_NAME/'$APP_DB_NAME'/g' local.config.php
sed -i 's/$DB_USER/'$APP_DB_USER'/g' local.config.php
sed -i 's/$DB_PASS/'$APP_DB_PASS'/g' local.config.php
sed -i 's/$DB_HOST/'$APP_DB_HOST'/g' local.config.php